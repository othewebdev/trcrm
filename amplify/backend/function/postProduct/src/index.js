const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const documentClient = new AWS.DynamoDB.DocumentClient();

const PRODUCT_TABLE = "Product-gg64scu6svdaxgv34zvlfad7me-dev";
const PRODUCT_TYPE = "Product";

const createProduct = async (payload) => {
  const { product_id, title, description, price, featured, image } = payload;
  var params = {
    TableName: PRODUCT_TABLE,
    Product: {
      id: product_id,
      title: title,
      __typename: PRODUCT_TYPE,
      description: description,
      isFeatured: featured,
      productImage: image,
      price: price,
    },
  };
  console.log(params);
  await documentClient.put(params).promise();
};

exports.handler = async (event) => {
  try {
    let payload = event.prev.result;
    payload.product_id = uuidv4();

    // Creates a product
    await createProduct(payload);

    return "SUCCESS";
  } catch (err) {
    console.log(err);
    return new Error(err);
  }
};
