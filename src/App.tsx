import Amplify from "aws-amplify";
import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
// Amplify Configuration
import awsExports from "./aws-exports";
import { getTheme } from "./core/theming/Theme";
import { ThemeProvider } from "./core/theming/ThemeContext";
import { AdminFlow } from "./flows/AdminFlow";
// Flows
import { HomeFlow } from "./flows/HomeFlow";
import { SuppliesFlow } from "./flows/SuppliesFlow";

Amplify.configure(awsExports);

function App() {
  const [isDark, setIsDark] = useState<boolean>(false);

  const toggleDark = () => {
    setIsDark(!isDark);
  };

  const tcmsTheme = getTheme({
    isDarkMode: isDark,
    themeName: "TruDefault",
  });

  return (
    <ThemeProvider theme={tcmsTheme}>
      <Router>
        <Routes>
          <Route path="/" element={<HomeFlow />} />
          <Route
            path="/products"
            element={<AdminFlow toggleDark={toggleDark} />}
          />
          <Route
            path="/supplies"
            element={<SuppliesFlow toggleDark={toggleDark} />}
          />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
