export const getProduct = /* GraphQL */ `
  query GetProduct($id: ID!) {
    getProduct(id: $id) {
      id
      name
      description
      price
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listProducts = /* GraphQL */ `
  query ListProducts(
    $filter: ModelProductFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProducts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        description
        price
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getSupplies = /* GraphQL */ `
  query GetSupplies($id: ID!) {
    getSupplies(id: $id) {
      id
      name
      description
      weight
      length
      width
      height
      supplierId
      material
      diameter
      color
      design
      unitOfMeasure
      cost
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listSupplies = /* GraphQL */ `
  query ListSupplies(
    $filter: ModelSuppliesFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSupplies(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        description
        weight
        length
        width
        height
        supplierId
        material
        diameter
        color
        design
        unitOfMeasure
        cost
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
