import { Storage } from "aws-amplify";
import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import config from "../../aws-exports";
import { postProduct } from "../../utils/products/productUtils";
import { TextInput } from "../common/TextInput";

export const AddProductForm: React.FC = () => {
  const {
    aws_user_files_s3_bucket_region: region,
    aws_user_files_s3_bucket: bucket,
  } = config;

  const [image, setImage] = useState("");

  const [productDetails, setProductDetails] = useState({
    name: "",
    description: "",
    price: 0,
  });

  const clearProduct = (e: any) => {
    e.preventDefault();
    setProductDetails({
      ...productDetails,
      name: "",
      description: "",
      price: 0,
    });
    setImage("");
  };

  const submit = (e: any) => {
    e.preventDefault();
    postProduct(productDetails);
  };

  const handleImageUpload = async (e: any) => {
    e.preventDefault();
    const file = e.target.files[0];
    const extension = file.name.split(".")[1];
    const name = file.name.split(".")[0];
    const key = `images/${uuidv4()}${name}.${extension}`;
    // const url = `https://${bucket}.s3.${region}.amazonaws.com/public/${key}`;
    try {
      // Upload the file to s3 with public access level.
      await Storage.put(key, file, {
        level: "public",
        contentType: file.type,
      });
      // Retrieve the uploaded file to display
      const image = await Storage.get(key, { level: "public" });
      setImage(image);
      setProductDetails({ ...productDetails });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <section>
        <h3>Create a Product</h3>
        <form className="form-wrapper" onSubmit={submit}>
          {image && <button onClick={clearProduct}>clear product</button>}
          <div className="form-image">
            {image ? (
              <img className="image-preview" src={image} alt="" />
            ) : (
              <TextInput
                type="file"
                accept="image/jpg"
                onChange={(e) => handleImageUpload(e)}
              />
            )}
          </div>
          <div className="form-fields">
            <div className="title-form">
              <p>
                <TextInput
                  label="Name"
                  type="title"
                  placeholder="Type the name"
                  onChange={(e) =>
                    setProductDetails({
                      ...productDetails,
                      name: e.target.value,
                    })
                  }
                  required
                />
              </p>
            </div>
            <div className="description-form">
              <p>
                <TextInput
                  isArea
                  label="description"
                  placeholder="Type the description of the product"
                  onChange={(e) =>
                    setProductDetails({
                      ...productDetails,
                      description: e.target.value,
                    })
                  }
                  required
                />
              </p>
            </div>
            <div className="price-form">
              <p>
                <TextInput
                  label="price"
                  type="text"
                  placeholder="Enter the price"
                  onChange={(e) =>
                    setProductDetails({
                      ...productDetails,
                      price: e.target.value,
                    })
                  }
                  required
                />
              </p>
            </div>
            <div className="submit-form">
              <button className="btn" type="submit">
                Submit
              </button>
            </div>
          </div>
        </form>
      </section>
    </div>
  );
};
