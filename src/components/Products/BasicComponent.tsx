import React from "react";
import { Button } from "../common/Button";

const BasicComponent = () => {
  return (
    <div>
      <p>We Need Products</p>
      <Button size="sm" icon="Icon" disabled variant="primary" />
      <Button size="lg" disabled label="Hello" variant="secondary" />
    </div>
  );
};

export default BasicComponent;
