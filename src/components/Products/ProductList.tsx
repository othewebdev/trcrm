import { useState } from "react";
import { useTheme } from "../../core/theming/useTheme";
import { editProduct, eraseProduct } from "../../utils/products/productUtils";
import { Button } from "../common/Button";
import { TextInput } from "../common/TextInput";

export interface ProductListProps {
  products: any[];
  toggleDark: () => void;
}

export const ProductList: React.FC<ProductListProps> = ({
  products,
  toggleDark,
}) => {
  const [selectedProducts, setSelectedProducts] = useState({} as any);
  const [isChecked, setIsChecked] = useState<Boolean>(false);
  const [isEditing, setIsEditing] = useState<Boolean>(false);
  const { colors } = useTheme();

  console.log(products);

  const [productDetails, setProductDetails] = useState({
    id: "",
    name: "",
    description: "",
    price: 0,
  });

  const handleCheckbox = (e: any, id: string) => {
    if (e.target.checked) {
      setIsChecked(true);
      products.map((p) => {
        if (p.id === id) {
          setSelectedProducts(p);
          setProductDetails({ ...productDetails, id: p.id });
        }
        return p;
      });
    } else {
      setIsChecked(!isChecked);
      return setSelectedProducts({});
    }
  };

  const deleteProd = () => {
    eraseProduct(selectedProducts.id);
  };

  const enableModal = () => {
    setIsEditing(!isEditing);
  };

  const submit = (e: any) => {
    e.preventDefault();
    console.log(productDetails);
    editProduct(productDetails);
    enableModal();
  };

  return (
    <div>
      <button onClick={toggleDark}>Dark mode</button>
      {isChecked && (
        <>
          <Button
            size="lg"
            variant="primary"
            label="delete"
            onClick={() => deleteProd()}
          />
          <Button
            size="lg"
            variant="secondary"
            label="edit"
            onClick={() => enableModal()}
          />
        </>
      )}
      {isEditing && isChecked && (
        <form onSubmit={submit}>
          <h3>Edit Product</h3>
          <TextInput
            label="Edit title"
            onChange={(e) =>
              setProductDetails({
                ...productDetails,
                name: e.target.value,
              })
            }
            required
          />
          <TextInput
            label="Edit description"
            onChange={(e) =>
              setProductDetails({
                ...productDetails,
                description: e.target.value,
              })
            }
            required
          />
          <TextInput
            label="Edit price"
            onChange={(e) =>
              setProductDetails({
                ...productDetails,
                price: e.target.value,
              })
            }
            required
          />
          <button className="btn" type="submit">
            Submit
          </button>
        </form>
      )}
      {products?.map((product: any) => (
        <div style={{ backgroundColor: colors.background }}>
          <input
            type="checkbox"
            name=""
            id=""
            value={product.id}
            onChange={(e) => handleCheckbox(e, product.id)}
          />
          <p style={{ color: colors.success }}>{product.name}</p>
          <p style={{ color: colors.error }}>{product.description}</p>
          <p style={{ color: colors.pending }}>{product.price}</p>
        </div>
      ))}
    </div>
  );
};
