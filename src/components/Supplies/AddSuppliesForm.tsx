import { Storage } from "aws-amplify";
import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import config from "../../aws-exports";
import { postSupply } from "../../utils/supplies/supplyUtils";
import { TextInput } from "../common/TextInput";

export const AddSuppliesForm: React.FC = () => {
  const {
    aws_user_files_s3_bucket_region: region,
    aws_user_files_s3_bucket: bucket,
  } = config;

  const [image, setImage] = useState("");

  const [suppliesDetails, setSuppliesDetails] = useState({
    name: "",
    description: "",
    weight: 0,
    length: 0,
    width: 0,
    height: 0,
    material: "",
    diameter: 0,
    color: "",
    design: "",
    unitOfMeasure: "",
    cost: 0,
  });

  const clearsupply = (e: any) => {
    e.preventDefault();
    setSuppliesDetails({
      ...suppliesDetails,
      name: "",
      description: "",
      weight: 0,
      length: 0,
      width: 0,
      height: 0,
      material: "",
      diameter: 0,
      color: "",
      design: "",
      unitOfMeasure: "",
      cost: 0,
    });
    setImage("");
  };

  const submit = (e: any) => {
    e.preventDefault();
    postSupply(suppliesDetails);
  };

  const handleImageUpload = async (e: any) => {
    e.preventDefault();
    const file = e.target.files[0];
    const extension = file.name.split(".")[1];
    const name = file.name.split(".")[0];
    const key = `images/${uuidv4()}${name}.${extension}`;
    // const url = `https://${bucket}.s3.${region}.amazonaws.com/public/${key}`;
    try {
      // Upload the file to s3 with public access level.
      await Storage.put(key, file, {
        level: "public",
        contentType: file.type,
      });
      // Retrieve the uploaded file to display
      const image = await Storage.get(key, { level: "public" });
      setImage(image);
      setSuppliesDetails({ ...suppliesDetails });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <section>
        <h3>Create a Supply</h3>
        <form className="form-wrapper" onSubmit={submit}>
          {image && <button onClick={clearsupply}>clear supply</button>}
          <div className="form-image">
            {image ? (
              <img className="image-preview" src={image} alt="" />
            ) : (
              <TextInput
                type="file"
                accept="image/jpg"
                onChange={(e) => handleImageUpload(e)}
              />
            )}
          </div>
          <div className="form-fields">
            <div className="title-form">
              <p>
                <TextInput
                  label="Name"
                  type="title"
                  placeholder="Type the name"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      name: e.target.value,
                    })
                  }
                  required
                />
              </p>
            </div>
            <div className="description-form">
              <p>
                <TextInput
                  isArea
                  label="description"
                  placeholder="Type the description of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      description: e.target.value,
                    })
                  }
                  required
                />
              </p>
            </div>
            <div className="length-form">
              <p>
                <TextInput
                  label="length"
                  placeholder="Type the length of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      length: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="width-form">
              <p>
                <TextInput
                  label="width"
                  placeholder="Type the width of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      width: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="height-form">
              <p>
                <TextInput
                  label="height"
                  placeholder="Type the height of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      height: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="material-form">
              <p>
                <TextInput
                  label="material"
                  placeholder="Type the material of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      material: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="diameter-form">
              <p>
                <TextInput
                  label="diameter"
                  placeholder="Type the diameter of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      diameter: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="color-form">
              <p>
                <TextInput
                  label="color"
                  placeholder="Type the color of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      color: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="design-form">
              <p>
                <TextInput
                  label="design"
                  placeholder="Type the design of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      design: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="unitOfMeasure-form">
              <p>
                <TextInput
                  label="Unit of measure"
                  placeholder="Type the unit of measure of the supply"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      unitOfMeasure: e.target.value,
                    })
                  }
                />
              </p>
            </div>
            <div className="cost-form">
              <p>
                <TextInput
                  label="cost"
                  type="text"
                  placeholder="Enter the cost"
                  onChange={(e) =>
                    setSuppliesDetails({
                      ...suppliesDetails,
                      cost: e.target.value,
                    })
                  }
                  required
                />
              </p>
            </div>
            <div className="submit-form">
              <button className="btn" type="submit">
                Submit
              </button>
            </div>
          </div>
        </form>
      </section>
    </div>
  );
};
