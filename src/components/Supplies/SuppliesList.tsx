import { useState } from "react";
import { useTheme } from "../../core/theming/useTheme";
import { editProduct, eraseProduct } from "../../utils/products/productUtils";
import { editSupply, eraseSupply } from "../../utils/supplies/supplyUtils";
import { Button } from "../common/Button";
import { TextInput } from "../common/TextInput";

export interface SuppliesListProps {
  supplies: any[];
  toggleDark: () => void;
}

export const SuppliesList: React.FC<SuppliesListProps> = ({
  supplies,
  toggleDark,
}) => {
  const [selectedSupplies, setSelectedSupplies] = useState({} as any);
  const [isChecked, setIsChecked] = useState<Boolean>(false);
  const [isEditing, setIsEditing] = useState<Boolean>(false);
  const { colors } = useTheme();

  const [suppliesDetails, setSuppliesDetails] = useState({
    id: "",
    name: "",
    description: "",
    weight: 0,
    length: 0,
    width: 0,
    height: 0,
    material: "",
    diameter: 0,
    color: "",
    design: "",
    unitOfMeasure: "",
    cost: 0,
  });

  const handleCheckbox = (e: any, id: string) => {
    if (e.target.checked) {
      setIsChecked(true);
      supplies.map((p) => {
        if (p.id === id) {
          setSelectedSupplies(p);
          setSuppliesDetails({ ...suppliesDetails, id: p.id });
        }
        return p;
      });
    } else {
      setIsChecked(!isChecked);
      return setSelectedSupplies({});
    }
  };

  const deleteProd = () => {
    eraseSupply(selectedSupplies.id);
  };

  const enableModal = () => {
    setIsEditing(!isEditing);
  };

  const submit = (e: any) => {
    e.preventDefault();
    editSupply(suppliesDetails);
    enableModal();
  };

  return (
    <div>
      <button onClick={toggleDark}>Dark mode</button>
      {isChecked && (
        <>
          <Button
            size="lg"
            variant="primary"
            label="delete"
            onClick={() => deleteProd()}
          />
          <Button
            size="lg"
            variant="secondary"
            label="edit"
            onClick={() => enableModal()}
          />
        </>
      )}
      {isEditing && isChecked && (
        <form onSubmit={submit}>
          <h3>Edit Product</h3>
          <TextInput
            label="Edit title"
            onChange={(e) =>
              setSuppliesDetails({
                ...suppliesDetails,
                name: e.target.value,
              })
            }
            required
          />
          <TextInput
            label="Edit description"
            onChange={(e) =>
              setSuppliesDetails({
                ...suppliesDetails,
                description: e.target.value,
              })
            }
            required
          />
          <TextInput
            label="Edit cost"
            onChange={(e) =>
              setSuppliesDetails({
                ...suppliesDetails,
                cost: e.target.value,
              })
            }
            required
          />
          <button className="btn" type="submit">
            Submit
          </button>
        </form>
      )}
      {supplies?.map((supply: any) => (
        <div style={{ backgroundColor: colors.background }}>
          <input
            type="checkbox"
            name=""
            id=""
            value={supply.id}
            onChange={(e) => handleCheckbox(e, supply.id)}
          />
          <p style={{ color: colors.success }}>{supply.name}</p>
          <p style={{ color: colors.error }}>{supply.description}</p>
          {supply.length !== 0 && (
            <p style={{ color: colors.error }}>{supply.length}</p>
          )}
          {supply.width !== 0 && (
            <p style={{ color: colors.error }}>{supply.width}</p>
          )}
          {supply.height !== 0 && (
            <p style={{ color: colors.error }}>{supply.height}</p>
          )}
          {supply.size && <p style={{ color: colors.error }}>{supply.size}</p>}
          {supply.color && (
            <p style={{ color: colors.error }}>{supply.color}</p>
          )}
          <p style={{ color: colors.pending }}>{supply.cost}</p>
        </div>
      ))}
    </div>
  );
};
