import React from "react";
import { ButtonProps } from "./types";
import { Link } from "react-router-dom";
import * as CSS from "csstype";
import { useTheme } from "../../../core/theming/useTheme";

export const Button: React.FC<ButtonProps> = ({
  label,
  onClick,
  disabled,
  variant,
  size,
  icon,
  isLink,
  linkURL,
}) => {
  const { colors } = useTheme();

  const Text: CSS.Properties = {
    color: colors.heading,
    textDecoration: "none",
  };

  const Disabled: CSS.Properties = {
    color: colors.disabledText,
    cursor: "default",
  };

  const Small: CSS.Properties = {
    ...Text,
    width: "46px",
    height: "46px",
    backgroundColor: colors.primary,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "6px",
    cursor: "pointer",
  };

  const Large: CSS.Properties = {
    ...Small,
    width: "350px",
    height: "46px",
    margin: "5px 0",
    backgroundColor: colors.secondary,
    borderRadius: "12px",
    transition: "all 0.5s",
  };

  const SmallSecondary: CSS.Properties = {
    ...Small,
    backgroundColor: colors.tertiary,
  };

  const DisabledSmall: CSS.Properties = {
    ...Small,
    ...Disabled,
    backgroundColor: colors.neutral,
  };

  const DisabledLarge: CSS.Properties = {
    ...Large,
    ...Disabled,
    backgroundColor: colors.neutral,
  };

  const handleStyleButton = (size: string, variant: string) => {
    if (disabled) {
      return size === "sm" ? DisabledSmall : DisabledLarge;
    } else if (size === "sm" && variant === "primary") {
      return Small;
    } else if (size === "sm" && variant === "secondary") {
      return SmallSecondary;
    }
    return Large;
  };

  const hasLabel = (size: string, label: string | undefined) => {
    if (size === "sm") {
      return "";
    }
    return label;
  };

  return isLink ? (
    <Link style={Text} to={linkURL || ""}>
      <div style={handleStyleButton(size, variant)} onClick={onClick}>
        <p>{hasLabel(size, label)}</p>
        {icon && <div>{icon}</div>}
      </div>
    </Link>
  ) : (
    <div style={handleStyleButton(size, variant)} onClick={onClick}>
      <p>{hasLabel(size, label)}</p>
      {icon && <div>{icon}</div>}
    </div>
  );
};
