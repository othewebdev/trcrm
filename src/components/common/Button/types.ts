export interface ButtonProps {
  /**
   * The text on the button
   */
  label?: string;
  /**
   *  Function that will be called when the button is clicked
   */
  onClick?: () => void;
  /**
   * Disabled state
   */
  disabled?: boolean;
  /**
   * Variant types for button styling
   */
  variant: "primary" | "secondary";
  /**
   * Determines the button size
   */
  size: "lg" | "md" | "sm";
  /**
   * Determines if the button will have an icon and what icon it will have
   */
  icon?: string;
  /**
   * Determines if button is a link
   */
  isLink?: boolean;
  /**
   * Sets the link URL
   */
  linkURL?: string;
}
