import * as CSS from "csstype";
import React from "react";
import { useTheme } from "../../../core/theming/useTheme";
import { TextInputProps } from "./types";

export const TextInput: React.FC<TextInputProps> = ({
  label,
  placeholder,
  required,
  type,
  accept,
  onChange,
  isArea,
}) => {
  const { colors } = useTheme();

  const Input: CSS.Properties = {
    maxWidth: "312px",
    minHeight: "24px",
    borderColor: colors.body,
    borderRadius: "3px",
    borderWidth: "0.5px",
    backgroundColor: colors.background,
  };

  const Area: CSS.Properties = {
    ...Input,
    maxHeight: "48px",
  };

  const Container: CSS.Properties = {
    display: "flex",
    flexDirection: "column",
  };

  const Label: CSS.Properties = {
    marginBottom: "5px",
    textTransform: "capitalize",
    color: colors.body,
  };

  const props = {
    style: Area,
    onChange: onChange,
    placeholder: placeholder,
    required: required,
    type: type,
    accept: accept,
  };

  return (
    <div style={Container}>
      <label style={Label}>{label}</label>
      {isArea ? <textarea {...props} /> : <input {...props} />}
    </div>
  );
};
