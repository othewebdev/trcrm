export interface TextInputProps {
  /**
   * Label for the text input
   */
  label?: string;
  /**
   * Required
   */
  required?: boolean;
  /**
   * Placeholder for text input field
   */
  placeholder?: string;
  /**
   *
   */
  type?: string;
  /**
   *
   */
  accept?: string;
  /**
   *
   */
  onChange: (e: any) => void;
  /**
   *
   */
  isArea?: boolean;
}
