import React from "react";
import { TextModuleProps } from "./types";

export const TextModule: React.FC<TextModuleProps> = ({
  heading,
  headingSecondary,
  caption,
}) => {
  return (
    <div>
      <h3>{heading}</h3>
      <h3>{headingSecondary && headingSecondary}</h3>
      <p>{caption && caption}</p>
    </div>
  );
};
