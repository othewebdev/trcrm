export interface TextModuleProps {
  /**
   * Required heading for text
   */
  heading: string;
  /**
   * Optional heading for text
   */
  headingSecondary?: string;
  /**
   * Caption for text
   */
  caption?: string;
}
