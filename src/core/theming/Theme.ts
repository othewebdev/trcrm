import { DarkTheme, TCMSTheme } from "./themes";
import { ThemeMap } from "./types";

export type Theme = {
  dark: boolean;
  colors: {
    primary: string;
    secondary: string;
    tertiary: string;
    background: string;
    heading: string;
    body: string;
    disabledText: string;
    success: string;
    pending: string;
    error: string;
    neutral: string;
    black: string;
  };
};

export type ColorType = keyof Theme["colors"];

const THEMES: ThemeMap = {
  TruDefault: {
    light: TCMSTheme,
    dark: DarkTheme,
  },
};

export type GetThemeArgs = {
  isDarkMode: boolean | undefined;
  themeName?: string;
};

export const getTheme = ({
  isDarkMode,
  themeName = "TruDefault",
}: GetThemeArgs): Theme => {
  const mode = isDarkMode ? "dark" : "light";

  return {
    ...THEMES[themeName][mode],
  };
};
