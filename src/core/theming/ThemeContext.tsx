import React, { createContext, FC } from "react";

import { Theme } from "./Theme";
import { TCMSTheme as DefaultTheme } from "./themes";
import { ThemeContextProps } from "./types";

export const ThemeContext = createContext<Theme>(DefaultTheme);

export const ThemeProvider: FC<
  ThemeContextProps & {
    children: React.ReactChild;
  }
> = ({ theme, children }) => {
  return (
    <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
  );
};
