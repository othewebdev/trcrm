import { Theme } from "../Theme";

export const DarkTheme: Theme = {
  dark: false,
  colors: {
    primary: "#0787f0",
    secondary: "#d1153e",
    tertiary: "#82c4fa",
    background: "#2e2e2e",
    heading: "#e5e5e5",
    body: "#cecece",
    disabledText: "#454545",
    success: "#d1eea2",
    pending: "#f3cd93",
    error: "#e4b1b1",
    neutral: "#e8edef",
    black: "#000",
  },
};
