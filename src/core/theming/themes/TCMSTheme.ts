import { Theme } from "../Theme";

export const TCMSTheme: Theme = {
  dark: false,
  colors: {
    primary: "#1c629c",
    secondary: "#c22f4e",
    tertiary: "#08518c",
    background: "#ffffff",
    heading: "#ffffff",
    body: "#454545",
    disabledText: "#454545",
    success: "#6f9d25",
    pending: "#db9f45",
    error: "#db4545",
    neutral: "#e8edef",
    black: "#000",
  },
};
