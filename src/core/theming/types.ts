import { Theme } from "./Theme";

export interface Dimensions {
  window: { width: number; height: number; scale: number; fontScale: number };
}

export type ThemeContextProps = {
  theme: Theme;
};

export type ThemeVariant = {
  [key in "dark" | "light"]: Theme;
};

export interface ThemeMap {
  [key: string]: ThemeVariant;
}

export type DarkModeContextProps = {
  isDarkMode: boolean;
};
