import { useContext } from "react";

import { ThemeContext } from "./ThemeContext";
import { Theme } from "./Theme";

export const useTheme = (): Theme => {
  const theme = useContext(ThemeContext);
  return theme;
};
