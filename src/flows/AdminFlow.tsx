import { Authenticator } from "@aws-amplify/ui-react";
import React, { useEffect, useState } from "react";
import { Button } from "../components/common/Button";
import { AddProductForm } from "../components/Products/AddProductForm";
import { ProductList } from "../components/Products/ProductList";
import { CheckIsLoggedIn, getProducts } from "../utils/products/productUtils";

export interface AdminFlowProps {
  toggleDark: () => void;
}

export const AdminFlow: React.FC<AdminFlowProps> = ({ toggleDark }) => {
  const [items, setItems] = useState<any[]>([]);

  useEffect(() => {
    getProducts()
      .then((items) => setItems(items))
      .catch((err) => {
        console.log("There was an error listing products:", err);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [CheckIsLoggedIn]);

  return (
    <div>
      <Button size="lg" variant="primary" isLink label="Go Home" linkURL="/" />
      <Authenticator>
        {({ signOut, user }) => (
          <main>
            <h1>Hello {user.username}</h1>
            <button onClick={signOut}>Sign out</button>
          </main>
        )}
      </Authenticator>

      {CheckIsLoggedIn() && (
        <>
          <Button
            variant="primary"
            size="lg"
            label="Go to Supplies"
            isLink
            linkURL="/supplies"
          />
          <AddProductForm />
          <ProductList products={items} toggleDark={toggleDark} />
        </>
      )}
    </div>
  );
};
