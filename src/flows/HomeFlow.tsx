import React from "react";
import { Button } from "../components/common/Button";
import { TextModule } from "../components/common/layout/TextModule";

export const HomeFlow: React.FC = () => {
  return (
    <div>
      <TextModule
        heading="Welcome Home"
        headingSecondary="We Intend To Do More Here"
      />
      <Button
        size="lg"
        variant="primary"
        isLink
        label="Go to Products"
        linkURL="/products"
      />
    </div>
  );
};
