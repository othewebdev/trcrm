import { Authenticator } from "@aws-amplify/ui-react";
import React, { useEffect, useState } from "react";
import { Button } from "../components/common/Button";
import { AddProductForm } from "../components/Products/AddProductForm";
import { ProductList } from "../components/Products/ProductList";
import { AddSuppliesForm } from "../components/Supplies/AddSuppliesForm";
import { SuppliesList } from "../components/Supplies/SuppliesList";
import { CheckIsLoggedIn } from "../utils/products/productUtils";
import { getSupplies } from "../utils/supplies/supplyUtils";

export interface SuppliesFlowProps {
  toggleDark: () => void;
}

export const SuppliesFlow: React.FC<SuppliesFlowProps> = ({ toggleDark }) => {
  const [items, setItems] = useState<any[]>([]);

  useEffect(() => {
    getSupplies()
      .then((items) => setItems(items))
      .catch((err) => {
        console.log("There was an error listing products:", err);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [CheckIsLoggedIn]);

  return (
    <div>
      <Button size="lg" variant="primary" isLink label="Go Home" linkURL="/" />
      <Authenticator>
        {({ signOut, user }) => (
          <main>
            <h1>Hello {user.username}</h1>
            <button onClick={signOut}>Sign out</button>
          </main>
        )}
      </Authenticator>

      {CheckIsLoggedIn() && (
        <>
          <Button
            variant="primary"
            size="lg"
            label="Go to Products"
            isLink
            linkURL="/products"
          />
          <AddSuppliesForm />
          <SuppliesList supplies={items} toggleDark={toggleDark} />
        </>
      )}
    </div>
  );
};
