/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const postProduct = /* GraphQL */ `
  mutation PostProduct($input: PostProductInput!) {
    postProduct(input: $input)
  }
`;
export const postSupplies = /* GraphQL */ `
  mutation PostSupplies($input: PostSuppliesInput!) {
    postSupplies(input: $input)
  }
`;
export const createProduct = /* GraphQL */ `
  mutation CreateProduct(
    $input: CreateProductInput!
    $condition: ModelProductConditionInput
  ) {
    createProduct(input: $input, condition: $condition) {
      id
      name
      description
      price
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateProduct = /* GraphQL */ `
  mutation UpdateProduct(
    $input: UpdateProductInput!
    $condition: ModelProductConditionInput
  ) {
    updateProduct(input: $input, condition: $condition) {
      id
      name
      description
      price
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteProduct = /* GraphQL */ `
  mutation DeleteProduct(
    $input: DeleteProductInput!
    $condition: ModelProductConditionInput
  ) {
    deleteProduct(input: $input, condition: $condition) {
      id
      name
      description
      price
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createSupplies = /* GraphQL */ `
  mutation CreateSupplies(
    $input: CreateSuppliesInput!
    $condition: ModelSuppliesConditionInput
  ) {
    createSupplies(input: $input, condition: $condition) {
      id
      name
      description
      weight
      length
      width
      height
      supplierId
      material
      diameter
      color
      design
      unitOfMeasure
      cost
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateSupplies = /* GraphQL */ `
  mutation UpdateSupplies(
    $input: UpdateSuppliesInput!
    $condition: ModelSuppliesConditionInput
  ) {
    updateSupplies(input: $input, condition: $condition) {
      id
      name
      description
      weight
      length
      width
      height
      supplierId
      material
      diameter
      color
      design
      unitOfMeasure
      cost
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteSupplies = /* GraphQL */ `
  mutation DeleteSupplies(
    $input: DeleteSuppliesInput!
    $condition: ModelSuppliesConditionInput
  ) {
    deleteSupplies(input: $input, condition: $condition) {
      id
      name
      description
      weight
      length
      width
      height
      supplierId
      material
      diameter
      color
      design
      unitOfMeasure
      cost
      createdAt
      updatedAt
      owner
    }
  }
`;
