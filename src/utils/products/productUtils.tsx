import { API, graphqlOperation } from "aws-amplify";
import {
  createProduct,
  updateProduct,
  deleteProduct,
} from "../../api/mutations";
import { listProducts } from "../../api/queries";
import useUserStatus from "../../utils/useUserStatus";

export type initialProductState = {
  name: string;
  description: string;
  price: number;
};

export const postProduct = async (state: initialProductState) => {
  try {
    if (!state.name || !state.price) return;
    await API.graphql(graphqlOperation(createProduct, { input: state }));
  } catch (err) {
    console.log("There was an error creating this product:", err);
  }
};

export const editProduct = async (state: initialProductState) => {
  try {
    await API.graphql(graphqlOperation(updateProduct, { input: state }));
  } catch (err) {
    console.log("There was an error editing this product:", err);
  }
};

export const eraseProduct = async (id: string) => {
  try {
    await API.graphql(graphqlOperation(deleteProduct, { input: { id } }));
  } catch (err) {
    console.log("There was an error deleting this product:", err);
  }
};

export const CheckIsLoggedIn = () => {
  const userStatus = useUserStatus();
  const isLoggedIn = null !== userStatus;

  return isLoggedIn;
};

export const getProducts = async () => {
  try {
    const { data }: any = await API.graphql({
      query: listProducts,
      authMode: "AMAZON_COGNITO_USER_POOLS",
    });
    const products = data.listProducts.items;

    return products;
  } catch (err) {
    console.log("There was an error listing products:", err);
  }
};
