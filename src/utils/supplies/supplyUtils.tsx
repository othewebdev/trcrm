import { API, graphqlOperation } from "aws-amplify";
import {
  createSupplies,
  updateSupplies,
  deleteSupplies,
} from "../../api/mutations";
import { listSupplies } from "../../api/queries";
import useUserStatus from "../useUserStatus";

export type initialProductState = {
  name: string;
  description: string;
  weight: number;
  length: number;
  width: number;
  height: number;
  material: string;
  diameter: number;
  color: string;
  design: string;
  unitOfMeasure: string;
  cost: number;
};

export const postSupply = async (state: initialProductState) => {
  try {
    if (!state.name || !state.cost) return;
    await API.graphql(graphqlOperation(createSupplies, { input: state }));
  } catch (err) {
    console.log("There was an error creating this supply:", err);
  }
};

export const editSupply = async (state: initialProductState) => {
  try {
    await API.graphql(graphqlOperation(updateSupplies, { input: state }));
  } catch (err) {
    console.log("There was an error editing this supply:", err);
  }
};

export const eraseSupply = async (id: string) => {
  try {
    await API.graphql(graphqlOperation(deleteSupplies, { input: { id } }));
  } catch (err) {
    console.log("There was an error deleting this supply:", err);
  }
};

export const CheckIsLoggedIn = () => {
  const userStatus = useUserStatus();
  const isLoggedIn = null !== userStatus;

  return isLoggedIn;
};

export const getSupplies = async () => {
  try {
    const { data }: any = await API.graphql({
      query: listSupplies,
      authMode: "AMAZON_COGNITO_USER_POOLS",
    });
    const supplies = data.listSupplies.items;

    return supplies;
  } catch (err) {
    console.log("There was an error listing supplies:", err);
  }
};
